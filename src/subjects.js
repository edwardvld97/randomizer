export const SUBJECTS = [
    {id: 0, message: 'Cum desfaci o banana? De ce nu se desface din celalalt capat?'},
    {id: 1, message: 'Sapun lichid VS sapun solid. Cine castiga si de ce?'},
    {id: 2, message: 'Cum se inmultesc girafele pitice?'},
    {id: 3, message: 'De ce nu exista mancare de pisici cu gust de soarece?'},
    {id: 4, message: 'De ce “sutien” este la singular iar “chiloți” este la plural?'},
    {id: 5, message: 'Cele 4 stomace ale unei vaci. Cum decurge procesul de digestie?'},
    {id: 6, message: 'Ce se afla in spatele unei gauri negre?'},
    {id: 7, message: 'Poți să plângi sub apă?'},
    {id: 8, message: 'Cine a aparut prima data, oul sau gaina? Argumenteaza.'},
    {id: 9, message: 'De ce pizza se livrează într-o cutie pătrată când ea este rotundă?'},
    {id: 10, message: 'Ananas pe pizza, pros and cons.'},
    {id: 11, message: 'Adevarul din spatele disparitiei Elodiei. Cum s-a intamplat totul?'},
    {id: 12, message: 'De ce toate frigiderele au un bec în ele însă nu există bec în niciun congelator?'},
    {id: 13, message: 'Cine a muscat din cascaval? Si mai important, de ce?'},
    {id: 14, message: 'Ce ai face cu un punct negru?'}
];
