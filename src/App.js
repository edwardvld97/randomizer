import './App.css';
import { SUBJECTS } from "./subjects";
import { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  let [selectedSubject, setSelectedSubject] = useState('No selection');
  let [wasHere, setWasHere] = useState('false');
  let [password, setPassword] = useState();
  const subjects = SUBJECTS;


  useEffect(() => {
    selectSubject();
    if (window.localStorage.getItem('wasHere') === 'true') {
      setWasHere('true');
    }
    window.localStorage.setItem('wasHere', 'true');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const random = (min, max) => Math.floor(Math.random() * (max - min)) + min;
  const findInArray = selectedId => subjects.find(sub => sub.id === selectedId).message;

  const selectSubject = () => {
    let randomNb = random( 0, 14);
    setSelectedSubject(findInArray(randomNb));
  }

  const resetVisits = () => {
    if (password === 'FlorinaBB123') {
      setWasHere('false');
      setPassword('');
    }
  }

  return (
      <Router>
        <Switch>
          <Route exact path='/' >
            <div className={wasHere === 'true' ? 'wrapper red': 'wrapper black'}>
              <div className="card">
                {selectedSubject}
              </div>
            </div>
            <br />
            <br />
            {wasHere === 'true' && <Link to={'/reset'}>
              <button className="btn">RESET</button>
            </Link>}
          </Route>
          <Route exact path='/reset'>
            <div>
              <br />
              <input value={password} onInput={e => setPassword(e.target.value)} type="password"/>
              <br />
              <button className="btn" onClick={resetVisits}>RESET</button>
              <Link to={'/'}>
                <button className="btn">BACK</button>
              </Link>
            </div>
          </Route>
        </Switch>
      </Router>
  );
}

export default App;
